import os
from openpyxl import Workbook

import library.SSH
from library import util
from library.Features import *


class Device:
    def __init__(self, deviceName, NEWorOLD):
        self.name = deviceName
        self.NEWorOLD = NEWorOLD
        #
        self.deviceDirPath = self.__getDeviceDirPath__()
        self.logDirPath = self.__getLogDirPath__()
        #
        self.shoRunFormal = self.__getShowRunFormal__()
        ###
        self.isis_obj = Isis(self.shoRunFormal, self.logDirPath)
        self.interface_obj = Interface(self.shoRunFormal, self.logDirPath)
        self.core_bgp_neighbor_obj = CoreBGPneighbor(self.shoRunFormal, self.logDirPath)
        self.edge_bgp_neighbor_obj = EdgeBGPneighbor(self.shoRunFormal, self.logDirPath)
        ###
        self.checkDic = {obj.getFeatureName():obj for obj in
            [check_class(self.shoRunFormal, self.logDirPath) for check_class in getFeaturesClassList()]}

    def __getDeviceDirPath__(self):
        return os.path.join(util.LOGDIR, self.name)

    def __getLogDirPath__(self):
        if self.NEWorOLD == util.OLD:
            return os.path.join(self.deviceDirPath, 'BEFORE')
        else:
            return os.path.join(self.deviceDirPath, 'AFTER')

    @staticmethod
    def getShowRunName(deviceName):
        return '{}_cfg.txt'.format(deviceName)

    @staticmethod
    def getShowRunFormalName(deviceName):
        return '{}_cfg_formal.txt'.format(deviceName)

    def __getShowRun__(self):
        os.makedirs(self.logDirPath, exist_ok=True)
        showRunFileName = self.getShowRunName(self.name)
        if not showRunFileName in os.listdir(self.logDirPath):
            showRun = library.SSH.getCFG(self.name)
            with open(os.path.join(self.logDirPath, showRunFileName), 'w') as f:
                f.write(showRun)
        else:
            with open(os.path.join(self.logDirPath, showRunFileName)) as f:
                showRun = f.read()
        return showRun

    def __getShowRunFormal__(self):
        def shoRun_toFormal(shoRunString):
            TAG = '\t '

            def __getPromp__(d):
                i = list(d.keys())
                i.sort()
                vector = list(map(lambda x: d[x], i))
                return TAG.join(vector) + TAG

            def __clearDicByKeys__(d, k):
                for i in list(d.keys()):
                    if i >= k:
                        del d[i]

            d = {}
            return_list = []
            for index, line in enumerate(shoRunString.splitlines()):
                if line.strip() == '!' or line.strip().upper() == 'EXIT':
                    continue
                i = re.search('\S', line)
                if i:
                    i = i.start()
                    __clearDicByKeys__(d, i)
                    d[i] = line
                    vector = __getPromp__(d)
                    return_list.append(vector)
            return '\n'.join(sorted(list(set(return_list))))

        showRun = self.__getShowRun__()
        showRunFormalFileName = self.getShowRunFormalName(self.name)
        if not showRunFormalFileName in os.listdir(self.logDirPath):
            with open(os.path.join(self.logDirPath, showRunFormalFileName), 'w') as f:
                showRunFormal = shoRun_toFormal(showRun)
                f.write(showRunFormal)
        else:
            with open(os.path.join(self.logDirPath, showRunFormalFileName)) as f:
                showRunFormal = f.read()
        return showRunFormal

    def downloadShowCommands(self):
        cmdList = []
        for obj in self.checkDic.values():
            cmdList = cmdList + obj.getCmdToDownloadTupleList()
        cmdList = [c[1] for c in cmdList ]
        #TODO: verificare se va bene
        cmdList = [c for c in cmdList if MotherCheck.getCmdName(c) not in os.listdir(self.logDirPath)]   ########### ?
        cmdList = sorted(list(set(cmdList)))
        #
        #download via SSH
        #
        if cmdList:
            for cmd, output in zip(cmdList, library.SSH.getCMDList(self.name, cmdList)):
                with open(os.path.join(self.logDirPath, MotherCheck.getCmdName(cmd)), 'w') as f:
                    f.write(output)

    def parseCommands(self):
        for obj in self.checkDic.values():
            obj.getParse()

    def parsedCommandsToSheets(self, workbook):
        for obj in self.checkDic.values():
            obj.getSheet(workbook)

    def compare(self, other):
        workbook = Workbook()
        #
        activeSheet = workbook.active
        selfNotInOther = sorted([line for line in self.shoRunFormal if line not in other.shoRunFormal])
        otherNotInSelf = sorted([line for line in other.shoRunFormal if line not in self.shoRunFormal])
        for line in [[''],[''],['']] + \
                [['in {} not in {}'.format(self.NEWorOLD, other.NEWorOLD)]] + selfNotInOther + [[''],[''],['']]+ \
                [['in {} not in {}'.format(other.NEWorOLD, self.NEWorOLD)]] + otherNotInSelf:
            activeSheet.append(line)
        #
        summarySheet = workbook.create_sheet('__summary__', index=1)
        for k in self.checkDic.keys():
            myObj = self.checkDic[k]
            otherObj = other.checkDic[k]
            tmpDic = myObj.compare__getSheet(otherObj, workbook, vectorMeaning=[self.NEWorOLD, other.NEWorOLD])
            for line in tmpDic.get('colorSetList'):
                summarySheet.append(line)
        workbook.save(os.path.join(self.deviceDirPath, 'Compare.xlsx'))