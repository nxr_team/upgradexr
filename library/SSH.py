import json
import time
import re

import paramiko
import sshtunnel
from paramiko_expect import SSHClientInteraction, strip_ansi_codes

#
from library import util



def getCFG(deviceName):
    credentials = getCredentials()

    # get CFG from SSH
    #bridge = Bridge(deviceName, *credentials)
    bridge = TunnelBridge(deviceName, **credentials)
    returnString = bridge.sendCmd('show running-config')
    returnString = returnString.replace('\r', '')

    return returnString

def getCMDList(deviceName, cmdList):
    credentials = getCredentials()

    # get CFG from SSH
    bridge = Bridge(deviceName, **credentials)
    returnStringList = bridge.sendCmdList(cmdList)

    return returnStringList


class Bridge:

    def __init__(self, deviceName, bridgeUsr, bridgePwd, tacacsUsr, tacacsPwd, bridgeIpAddress):
        self.deviceName = deviceName.upper()
        self.bridgeUsr = bridgeUsr
        self.bridgePwd = bridgePwd
        self.tacacsUsr = tacacsUsr
        self.tacacsPwd = tacacsPwd
        self.bridgeIpAddress = bridgeIpAddress

        self.prompt = '$'
        self.client = None

    def __connect__(self):
        self.client = paramiko.SSHClient()
        self.prompt = '.*$.*'
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(self.bridgeIpAddress, port=22, username=self.bridgeUsr, password=self.bridgePwd)
        #self.client = [self.client, SSHClientInteraction(self.client, timeout=120, display=False, buffer_size=999999)]
        self.client = [self.client, SSHClientInteraction_Mario(self.client, timeout=120, display=False, buffer_size=999999)]
        self.client[1].expect(self.prompt)
        self.client[1].send('ssh -l ' + self.tacacsUsr + ' ' + self.deviceName)
        index = self.client[1].expect(['.*assword.*', '.*Are you sure you want to continue connecting.*'])
        if index == 1:
            self.client[1].send('yes')
            self.client[1].expect('.*assword.*')
        self.client[1].send(self.tacacsPwd)
        self.prompt = self.deviceName + '#'
        if 'VCE' in self.deviceName or 'VSW' in self.deviceName:
            self.prompt = self.prompt + ' '
        elif 'VPE' in self.deviceName:
            self.prompt = '.*' + self.deviceName + '#.*'
        else:
            self.prompt = '.*' + self.deviceName + '#.*'
        str = self.client[1].expect(self.prompt)
        self.__sendCmd__('terminal len 0')

    def __disconnect__(self):
        self.client[1].close()
        self.client[0].close()
        self.client = None

    def __sendCmd__(self, command):
        print(self.deviceName,'\t-----sendCommand-----\t',repr(command))
        command = '\n'.join([c.strip() for c in command.splitlines() if c.strip()])
        self.client[1].send(command)
        self.client[1].expect(self.prompt)
        out = self.client[1].current_output
        return out

    def sendCmdList(self, cmdStrList):
        self.__connect__()
        outStrList = [self.__sendCmd__(cmd) for cmd in cmdStrList]
        self.__disconnect__()
        return outStrList

    def sendCmd(self, cmdStr):
        return self.sendCmdList([cmdStr])[0]


class SSHClientInteraction_Mario(SSHClientInteraction):
    def expect(self, re_strings='', timeout=None, output_callback=None, default_match_prefix='.*\n', strip_ansi=True):
        """
        This function takes in a regular expression (or regular expressions)
        that represent the last line of output from the server.  The function
        waits for one or more of the terms to be matched.  The regexes are
        matched using expression \n<regex>$ so you'll need to provide an
        easygoing regex such as '.*server.*' if you wish to have a fuzzy
        match.

        :param re_strings: Either a regex string or list of regex strings
                           that we should expect; if this is not specified,
                           then EOF is expected (i.e. the shell is completely
                           closed after the exit command is issued)
        :param timeout: Timeout in seconds.  If this timeout is exceeded,
                        then an exception is raised.
        :param output_callback: A function used to print ssh output. Printed to stdout
                                by default. A user-defined logger may be passed like
                                output_callback=lambda m: mylog.debug(m)
        :param default_match_prefix: A prefix to all match regexes, defaults to '.*\n',
                                     can set to '' on cases prompt is the first line,
                                     or the command has no output.
        :param strip_ansi: If True, will strip ansi control chars befores regex matching
                           default to True.
        :return: An EOF returns -1, a regex metch returns 0 and a match in a
                 list of regexes returns the index of the matched string in
                 the list.
        :raises: A socket.timeout exception is raised on timeout.
        """

        output_callback = output_callback if output_callback else self.output_callback
        # Set the channel timeout
        timeout = timeout if timeout else self.timeout
        self.channel.settimeout(timeout)

        # Create an empty output buffer
        self.current_output = ''

        # This function needs all regular expressions to be in the form of a
        # list, so if the user provided a string, let's convert it to a 1
        # item list.
        if isinstance(re_strings, str) and len(re_strings) != 0:
            re_strings = [re_strings]

        # Loop until one of the expressions is matched or loop forever if
        # nothing is expected (usually used for exit)
        while (
            len(re_strings) == 0 or
            not [re_string
                 for re_string in re_strings
                 if re.match(default_match_prefix + re_string + '$',
                             self.current_output, re.DOTALL)]
        ):
            # Read some of the output
            current_buffer = self.channel.recv(self.buffer_size)

            # If we have an empty buffer, then the SSH session has been closed
            if len(current_buffer) == 0:
                break

            # Convert the buffer to our chosen encoding
            current_buffer_decoded = current_buffer.decode(self.encoding, errors='ignore')

            # Strip all ugly \r (Ctrl-M making) characters from the current
            # read
            current_buffer_decoded = current_buffer_decoded.replace('\r', '')

            # Display the current buffer in realtime if requested to do so
            # (good for debugging purposes)
            if self.display:
                output_callback(current_buffer_decoded)

            if strip_ansi:
                current_buffer_decoded = strip_ansi_codes(current_buffer_decoded)

            # Add the currently read buffer to the output
            self.current_output += current_buffer_decoded

        # Grab the first pattern that was matched
        if len(re_strings) != 0:
            found_pattern = [(re_index, re_string)
                             for re_index, re_string in enumerate(re_strings)
                             if re.match(default_match_prefix + re_string + '$',
                                         self.current_output, re.DOTALL)]

        # Clean the output up by removing the sent command
        self.current_output_clean = self.current_output
        if len(self.current_send_string) != 0:
            self.current_output_clean = (
                self.current_output_clean.replace(
                    self.current_send_string + '\n', ''
                )
            )

        # Reset the current send string to ensure that multiple expect calls
        # don't result in bad output cleaning
        self.current_send_string = ''

        # Clean the output up by removing the expect output from the end if
        # requested and save the details of the matched pattern
        if len(re_strings) != 0 and len(found_pattern) != 0:
            self.current_output_clean = (
                re.sub(
                    found_pattern[0][1] + '$', '', self.current_output_clean
                )
            )
            self.last_match = found_pattern[0][1]
            return found_pattern[0][0]
        else:
            # We would socket timeout before getting here, but for good
            # measure, let's send back a -1
            return -1

class TunnelBridge:
    def __init__(self, deviceName, bridgeUsr, bridgePwd, tacacsUsr, tacacsPwd, bridgeIpAddress):
        self.deviceName = deviceName.upper()
        self.bridgeUsr = bridgeUsr
        self.bridgePwd = bridgePwd
        self.tacacsUsr = tacacsUsr
        self.tacacsPwd = tacacsPwd
        self.bridgeIpAddress = bridgeIpAddress

    def sendCmd(self, cmdStr):
        x = ''
        with sshtunnel.open_tunnel(
                (self.bridgeIpAddress, 22),
                ssh_username = self.bridgeUsr,
                ssh_password = self.bridgePwd,
                remote_bind_address=(self.deviceName, 22),
                local_bind_address=('0.0.0.0', 10022)
        ) as tunnel:
            with paramiko.SSHClient() as ssh:
                ssh.load_system_host_keys()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect('localhost',
                            port=10022,
                            username=self.tacacsUsr,
                            password=self.tacacsPwd,
                            allow_agent=False,look_for_keys=False)
                ssh.exec_command('terminal len 0')
                print(self.deviceName, '\t-----sendCommand-----\t', repr(cmdStr))
                stdin, stdout, stderr = ssh.exec_command(cmdStr)
                x = stdout.read().decode('ascii', errors='ignore')
                #time.sleep(1)
                ssh.close()
                time.sleep(5)
        return x



def getCredentials():
    with open(util.CREDENTIALS_FILE) as f:
        return json.loads(f.read())

