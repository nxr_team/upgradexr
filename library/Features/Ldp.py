import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Ldp(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[LDP]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getCommandClassList(self):
        return [ShowMplsLdpNeighbor, ShowMplsLdpNeighborBrief]

######################################################################################################
######################################################################################################

class ShowMplsLdpNeighbor(Ldp, MotherCommand):
    '''
    solo log
    '''
    def __init__(self):
        self.tableHeader = []
        self.contentDic = {}

    def getCommand(self):
        return 'show mpls ldp neighbor'

######################################################################################################
######################################################################################################

class ShowMplsLdpNeighborBrief(Ldp, MotherCommand):
    def __init__(self):
        self.tableHeader = ['peer', 'gr', 'nsr', 'discovery_ipv4', 'discovery_ipv6', 'addresses_ipv4', 'addresses_ipv6', 'labels_ipv4', 'labels_ipv6']
        self.contentDic = {}

    def getCommand(self):
        return 'show mpls ldp neighbor brief'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            for line in re.findall('^\d+[.]\S+:\d.*', cmdStr, re.M):
                '''
10.176.0.124:0     Y   Y    4d09h       1     0     17    0     987    0                  
                '''
                line = line.split()
                peer = line[0]
                gr = line[1]
                nsr = line[2]
                discovery_ipv4 = line[4]
                discovery_ipv6 = line[5]
                addresses_ipv4 = line[6]
                addresses_ipv6 = line[7]
                labels_ipv4 = line[8]
                labels_ipv6 = line[9]
                lineDic = {'peer':peer, 'gr':gr, 'nsr':nsr,
                           'discovery_ipv4':discovery_ipv4, 'discovery_ipv6':discovery_ipv6,
                           'addresses_ipv4':addresses_ipv4, 'addresses_ipv6':addresses_ipv6,
                           'labels_ipv4':labels_ipv4, 'labels_ipv6':labels_ipv6}
                self.contentDic[peer] = lineDic

