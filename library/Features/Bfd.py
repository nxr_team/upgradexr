import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Bfd(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[BFD]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getCommandClassList(self):
        return [ShowBfdSession, ShowBfdClient]



######################################################################################################
######################################################################################################

class ShowBfdSession(Bfd, MotherCommand):
    def __init__(self):
        self.tableHeader = ['source', 'destinationAddress', 'state']
        self.contentDic = {}

    def getCommand(self):
        return 'show bfd session'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            for line in re.findall('^\S+\s+[\d.]+\s.*\n.*', cmdStr, re.M):
                if re.search('^\d', line, re.M):
                    line = re.search('^(\S+)\s+(\S+)\s+.*\s+(\S+)\s+$', line.replace('\n', ' '), re.M).groups()
                else:
                    line = re.search('^(\S+)\s+(\S+)\s+.*\s+(\S+)\s+$', line.splitlines()[0], re.M).groups()
                source = line[0]
                destinationAddress = line[1]
                state = line[2]
                lineDic = {'source':source, 'destinationAddress':destinationAddress, 'state':state}
                self.contentDic[source] = lineDic

######################################################################################################
######################################################################################################

class ShowBfdClient(Bfd, MotherCommand):
    def __init__(self):
        self.tableHeader = ['name', 'numSessions']
        self.contentDic = {}

    def getCommand(self):
        return 'show bfd client'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            '''
show bfd client
Mon Feb  3 11:00:37.544 MET
Name                 Node       Num sessions  
-------------------- ---------- --------------
L2VPN_ATOM           0/RP0/CPU0 0             
MPLS-TE              0/RP0/CPU0 0             
bgp-default          0/RP0/CPU0 0             
bundlemgr_distrib    0/RP0/CPU0 50            
isis-CORE            0/RP0/CPU0 4             
object_tracking      0/RP0/CPU0 0             
pim6                 0/RP0/CPU0 0             
pim                  0/RP0/CPU0 0             
rsvp                 0/RP0/CPU0 0             
RP/0/RP0/CPU0:BOVPE111#             
            '''
            for line in re.findall('^(\S+)\s+0/RP\d/CPU\d\s+(\d+)', cmdStr, re.M):
                name = line[0]
                numSessions = line[1]
                lineDic = {'name':name, 'numSessions':numSessions}
                self.contentDic[name] = lineDic