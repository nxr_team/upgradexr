import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *

class Interface:
    def __init__(self, showRunFormal, logDir):
        self.logDir = logDir
        self.interfacesList = self.searchActiveInterface__(showRunFormal)


    def searchActiveInterface__(self, showRunFormal):
        interfacesList = []
        for i in sorted(list(set(re.findall('^interface (\S+)', showRunFormal, re.M)))):
            if 'Mgmt' in i or 'tunnel' in i or 'Loopback' in i or '.' in i or 'preconfigure' in i:
                continue
            if re.search('^interface {}\s+shutdown'.format(i), showRunFormal, re.M):
                # print('interface {} is already shutdown'.format(i))
                continue
            if re.search('^interface {}\s+bundle id'.format(i), showRunFormal, re.M):
                # print('interface {} is a subif'.format(i))
                continue
            interfacesList.append(i)
        return interfacesList

    def getShutCmds(self):
        return ['interface {} shutdown'.format(i) for i in self.interfacesList]

    def getNoShutCmds(self):
        return ['no interface {} shutdown'.format(i) for i in self.interfacesList]
