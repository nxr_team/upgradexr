import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Isis(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[ISIS]'
        self.logDir = logDir
        self.processNameList = sorted(list(set(re.findall('^router isis (\S+)\s+net', showRunFormal, re.M))))
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getShutCmds(self):
        return ['router isis {} set-overload-bit'.format(i) for i in self.processNameList]

    def getNoShutCmds(self):
        return ['router isis {} set-overload-bit on-startup 600'.format(i) for i in self.processNameList]


    def getCommandClassList(self):
        return [ShowIsisNeighbor, ShowIsIsDatabase]



######################################################################################################
######################################################################################################

class ShowIsisNeighbor(Isis, MotherCommand):
    def __init__(self):
        self.tableHeader = ['systemId', 'interface', 'state', 'type']
        self.contentDic = {}

    def getCommand(self):
        return 'show isis neighbor'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStr = cmdStr.split('\n\n')[1]
            '''
IS-IS CORE neighbors:
System Id      Interface        SNPA           State Holdtime Type IETF-NSF
BOVAR101       BE20             *PtoP*         Up    25       L2   Capable
BOGSR201       BE301            *PtoP*         Up    22       L2   Capable
            '''
            for line in cmdStr.splitlines()[2:]:
                line = line.split()
                systemId = line[0]
                interface = line[1]
                state = line[3]
                type = line[5]
                lineDic = {'systemId': systemId, 'interface': interface, 'state': state, 'type': type}
                self.contentDic[systemId] = lineDic


######################################################################################################
######################################################################################################

class ShowIsIsDatabase(Isis, MotherCommand):
    def __init__(self):
        self.tableHeader = ['lspid', 'level', 'flags']
        self.contentDic = {}

    def getCommand(self):
        return 'show isis database'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            for cmdStrLevel in cmdStr.split('(L'):
                '''
IS-IS CORE (Level-2) Link State Database
LSPID                 LSP Seq Num  LSP Checksum  LSP Holdtime  ATT/P/OL
MIGSR102.00-00        0x00001083   0xfe21        53213           0/0/0
MIGSR102.00-01        0x00000f15   0x8ff0        59476           0/0/0
                '''
                if not re.search('^evel', cmdStrLevel, re.M):
                    continue
                level = 'L' + re.search('^(evel\-\d)', cmdStrLevel, re.M).group(0)
                for line in re.findall('.*\d/\d/\d$', cmdStrLevel, re.M):
                    line = line.split()
                    lspid = line[0]
                    flags = line[-1]
                    lineDic = {'level':level, 'lspid':lspid, 'flags':flags}
                    self.contentDic[(level, lspid)] = lineDic


