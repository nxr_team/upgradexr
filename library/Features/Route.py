import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Route(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[ROUTE]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getCommandClassList(self):
        return [ShowRouteAfiAllSummary, ShowRouteVrfAllAfiAllSummary]

    def compare(self, other):
        keys = sorted(self.contentDic.keys())
        resultsCompareList = []
        myList = []
        otherList = []
        for k in keys:
            if k in other.contentDic:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append(self.__dic2list__(other.contentDic[k], self.tableHeader))
                if self.contentDic[k] == other.contentDic[k]:
                    resultsCompareList.append(GREEN)
                elif 0.99*int(self.contentDic[k]['routes']) < int(other.contentDic[k]['routes']) < 1.01*int(self.contentDic[k]['routes']):
                    resultsCompareList.append(YELLOW)
                else:
                    resultsCompareList.append(RED)
            else:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append([])
                resultsCompareList.append(RED)
        return resultsCompareList, myList, otherList

######################################################################################################
######################################################################################################

class ShowRouteAfiAllSummary(Route, MotherCommand):
    def __init__(self):
        self.tableHeader = ['afi', 'source', 'routes']
        self.contentDic = {}

    def getCommand(self):
        return 'show route afi-all summary'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            afiList = re.findall('^(IPv\d \S+):', cmdStr, flags=re.M)
            for afi, output in zip(afiList, re.split('^IPv\d Unicast:', cmdStr, flags=re.M)[1:]):
                for line in re.findall('^(.*)\s+(\d+)\s+\d+\s+\d+\s+\d+\s+', output, re.M):
                    source = line[0]
                    routes = line[1]
                    lineDic = {'afi':afi, 'source':source, 'routes':routes}
                    self.contentDic[(afi, source)] = lineDic

######################################################################################################
######################################################################################################

class ShowRouteVrfAllAfiAllSummary(Route, MotherCommand):
    def __init__(self):
        self.tableHeader = ['vrfNAme', 'afi', 'source', 'routes']
        self.contentDic = {}

    def getCommand(self):
        return 'show route vrf all afi-all summary'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            for vrfOutput in cmdStr.split('VRF:'):
                vrfName = vrfOutput.splitlines()[0].strip()
                afiList = re.findall('^(IPv\d \S+):', vrfOutput, flags=re.M)
                for afi, output in zip(afiList,
                                       re.split('^IPv\d Unicast:', vrfOutput, flags=re.M)[1:]):
                    for line in re.findall('^(.*)\s+(\d+)\s+\d+\s+\d+\s+\d+\s+', output, re.M):
                        source = line[0]
                        routes = line[1]
                        lineDic = {'vrfNAme':vrfName, 'afi': afi, 'source': source, 'routes': routes}
                        self.contentDic[(vrfName, afi, source)] = lineDic

