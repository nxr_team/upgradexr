import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Lacp(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[LACP]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getCommandClassList(self):
        return [ShowLacp]



######################################################################################################
######################################################################################################

class ShowLacp(Lacp, MotherCommand):
    def __init__(self):
        self.tableHeader = ['bundleName',
                            'local_portName', 'local_rate', 'local_state',
                            'partner_rate', 'partner_state']
        self.contentDic = {}

    def getCommand(self):
        return 'show lacp'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStr = re.sub('^Bundle-Ether', '@#@Bundle-Ether', cmdStr, flags=re.M)
            for bundleStr in re.split('^@#@', cmdStr, flags=re.M)[1:]:
                '''
Bundle-Ether451

  Port          (rate)  State    Port ID       Key    System ID
  --------------------  -------- ------------- ------ ------------------------
Local
  Te0/0/0/18       30s  ascdA--- 0x8000,0x0023 0x01c3 0x8000,78-ba-f9-3f-ef-bc
   Partner         30s  ascdA--- 0x8000,0x0020 0x01c3 0x8000,10-f3-11-36-6c-04
  Te0/0/0/19       30s  ascdA--- 0x8000,0x0022 0x01c3 0x8000,78-ba-f9-3f-ef-bc
   Partner         30s  ascdA--- 0x8000,0x0021 0x01c3 0x8000,10-f3-11-36-6c-04
  Te0/0/0/20       30s  ascdA--- 0x8000,0x0020 0x01c3 0x8000,78-ba-f9-3f-ef-bc
   Partner         30s  ascdA--- 0x8000,0x0036 0x01c3 0x8000,10-f3-11-36-6c-04
  Te0/0/0/21       30s  ascdA--- 0x8000,0x001f 0x01c3 0x8000,78-ba-f9-3f-ef-bc
   Partner         30s  ascdA--- 0x8000,0x0035 0x01c3 0x8000,10-f3-11-36-6c-04
  Te0/0/0/22       30s  ascdA--- 0x8000,0x0009 0x01c3 0x8000,78-ba-f9-3f-ef-bc
   Partner         30s  ascdA--- 0x8000,0x0034 0x01c3 0x8000,10-f3-11-36-6c-04
  Te0/0/0/23       30s  ascdA--- 0x8000,0x0008 0x01c3 0x8000,78-ba-f9-3f-ef-bc
   Partner         30s  ascdA--- 0x8000,0x0033 0x01c3 0x8000,10-f3-11-36-6c-04

  Port                  Receive    Period Selection  Mux       A Churn P Churn
  --------------------  ---------- ------ ---------- --------- ------- -------
Local
  Te0/0/0/18            Current    Slow   Selected   Distrib   None    None   
  Te0/0/0/19            Current    Slow   Selected   Distrib   None    None   
  Te0/0/0/20            Current    Slow   Selected   Distrib   None    None   
  Te0/0/0/21            Current    Slow   Selected   Distrib   None    None   
  Te0/0/0/22            Current    Slow   Selected   Distrib   None    None   
  Te0/0/0/23            Current    Slow   Selected   Distrib   None    None               
                '''
                bundleName = bundleStr.splitlines()[0].strip()
                interfacesList = re.findall('^\s+(\S+[\d/]+)\s+\d+s', bundleStr, re.M)
                for i in interfacesList:
                    local_portName = i
                    x = re.search(i + '\s+.*\n.*', bundleStr, re.M).group(0)
                    local_rate, local_state = re.search('{}\s+(\S+)\s+(\S+)\s+'.format(i), x, re.M).groups()
                    partner_rate, partner_state = re.search('{}\s+(\S+)\s+(\S+)\s+'.format('Partner'), x, re.M).groups()
                    lineDic = {'bundleName':bundleName,
                            'local_portName':local_portName, 'local_rate':local_rate, 'local_state':local_state,
                            'partner_rate':partner_rate, 'partner_state':partner_state}
                    self.contentDic[(bundleName, local_portName)] = lineDic