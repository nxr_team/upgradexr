import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Bgp(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[BGP]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getShutCmds(self):
        pass

    def getNoShutCmds(self):
        pass

    def getCommandClassList(self):
        return [ShowBgpAllAllSummary, ShowBgpVrfAllIpv4UnicastSummary, ShowBgpVrfAllIpv6UnicastSummary]

    def compare(self, other):
        keys = sorted(self.contentDic.keys())
        resultsCompareList = []
        myList = []
        otherList = []
        for k in keys:
            if k in other.contentDic:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append(self.__dic2list__(other.contentDic[k], self.tableHeader))
                if self.contentDic[k] == other.contentDic[k]:
                    resultsCompareList.append(GREEN)
                elif 0.99*int(self.contentDic[k]['St/PfxRcd']) < int(other.contentDic[k]['St/PfxRcd']) < 1.01*int(self.contentDic[k]['St/PfxRcd']):
                    resultsCompareList.append(YELLOW)
                else:
                    resultsCompareList.append(RED)
            else:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append([])
                resultsCompareList.append(RED)
        return resultsCompareList, myList, otherList

class CoreBGPneighbor(Bgp):
    def __init__(self, showRunFormal, logDir):
        '''
        neighborList: list of ip neighbor address
        '''
        super()
        self.neighborGroupList = sorted(list(set(re.findall('^router bgp \d+\s+neighbor-group (\S+)', showRunFormal, re.M))))

    def getShutCmds(self):
        return ['router bgp 30722 neighbor-group {} shutdown'.format(i) for i in self.neighborGroupList]

    def getNoShutCmds(self):
        return ['no router bgp 30722 neighbor-group {} shutdown'.format(i) for i in self.neighborGroupList]

class EdgeBGPneighbor(Bgp):
    def __init__(self, showRunFormal, logDir):
        '''
        neighborList: list of tuple(vrf, ip neighbor address)
        '''
        super()
        self.neighborList = re.findall('^router bgp .* vrf (\S+)\s+ neighbor (\S+)\s+$', showRunFormal, re.M) + \
                            [(None, x) for x in re.findall('^router bgp \d+\s+ neighbor (\S+)\s+$', showRunFormal, re.M)
                             if x not in re.findall('^router bgp \d+\s+ neighbor (\S+)\s+use neighbor-group', showRunFormal, re.M) ]

    def getShutCmds(self):
        returnListCmd = []
        for i in self.neighborList:
            vrf, neiIP = i
            if vrf:
                returnListCmd.append('router bgp 30722 vrf {} neighbor {} shutdown'.format(vrf, neiIP))
            else:
                returnListCmd.append('router bgp 30722 neighbor {} shutdown'.format(neiIP))
        return returnListCmd

    def getNoShutCmds(self):
        returnListCmd = []
        for i in self.neighborList:
            vrf, neiIP = i
            if vrf:
                returnListCmd.append('no router bgp 30722 vrf {} neighbor {} shutdown'.format(vrf, neiIP))
            else:
                returnListCmd.append('no router bgp 30722 neighbor {} shutdown'.format(neiIP))
        return returnListCmd


######################################################################################################
######################################################################################################

class ShowBgpAllAllSummary(Bgp, MotherCommand):
    def __init__(self):
        self.tableHeader = ['addressFamily', 'neighborIp', 'St/PfxRcd']
        self.contentDic = {}

    def getCommand(self):
        return 'show bgp all all summary'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStr = cmdStr.split('\nAddress Family: ')[1:]
            for af_output_str in cmdStr:
                addressFamily = af_output_str.splitlines()[0].strip()
                '''
VPNv4 Unicast
-----------------------------

BGP router identifier 10.176.1.13, local AS number 30722
BGP generic scan interval 60 secs
Non-stop routing is enabled
BGP table state: Active
Table ID: 0x0   RD version: 0
BGP main routing table version 3541758885
BGP NSR Initial initsync version 6643530 (Reached)
BGP NSR/ISSU Sync-Group versions 3541758885/0
BGP scan interval 60 secs

BGP is operating in STANDALONE mode.


Process       RcvTblVer   bRIB/RIB   LabelVer  ImportVer  SendTblVer  StandbyVer
Speaker       3541758885  3541758885  3541758885  3541758885  3541758885  3541758885

Neighbor        Spk    AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down  St/PfxRcd
10.176.2.20       0 30722 1812328724 1320074 3541758885    0    0    2y01w      71639
10.176.2.77       0 30722 1719701585 1319943 3541758885    0    0     8w2d      71639
10.176.2.141      0 30722 1633197396 1320093 3541758885    0    0    1y04w      71639
10.176.2.207      0 30722 1747348965 1320126 3541758885    0    0    1y11w      71639


                '''
                for line in re.findall('^[\d.]+.*', af_output_str, re.M):
                    line = line.split()
                    neighborIp = line[0]
                    st_pfxRcd = line[-1]
                    lineDic = {'addressFamily':addressFamily, 'neighborIp':neighborIp, 'St/PfxRcd':st_pfxRcd}
                    self.contentDic[(addressFamily, neighborIp)] = lineDic

######################################################################################################
######################################################################################################

class ShowBgpVrfAllIpv4UnicastSummary(Bgp, MotherCommand):
    def __init__(self):
        self.tableHeader = ['vrf', 'neighborIp', 'St/PfxRcd']
        self.contentDic = {}

    def getCommand(self):
        return 'show bgp vrf all ipv4 unicast summary'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStr = cmdStr.split('\nVRF: ')[1:]
            for vrf_output_str in cmdStr:
                vrf = vrf_output_str.splitlines()[0].strip()
                '''
DSL_DATA
-------------
BGP VRF DSL_DATA, state: Active
BGP Route Distinguisher: 30722:1003201111
VRF ID: 0x60000002
BGP router identifier 10.176.1.13, local AS number 30722
Non-stop routing is enabled
BGP table state: Active
Table ID: 0xe0000001   RD version: 3541762305
BGP main routing table version 3541762306
BGP NSR Initial initsync version 6643530 (Reached)
BGP NSR/ISSU Sync-Group versions 3541762306/0

BGP is operating in STANDALONE mode.


Process       RcvTblVer   bRIB/RIB   LabelVer  ImportVer  SendTblVer  StandbyVer
Speaker       3541762306  3541762306  3541762306  3541762306  3541762306  3541762306

Neighbor        Spk    AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down  St/PfxRcd
83.224.40.181     0 30722 2761507 1664684 3541762306    0    0    1d18h          1
83.224.46.114     0 30722 1027586 1201164 3541762306    0    0    1d18h          0


                '''
                for line in re.findall('^[\d.]+.*', vrf_output_str, re.M):
                    line = line.split()
                    neighborIp = line[0]
                    st_pfxRcd = line[-1]
                    lineDic = {'vrf':vrf, 'neighborIp':neighborIp, 'St/PfxRcd':st_pfxRcd}
                    self.contentDic[(vrf, neighborIp)] = lineDic

######################################################################################################
######################################################################################################

class ShowBgpVrfAllIpv6UnicastSummary(Bgp, MotherCommand):
    def __init__(self):
        self.tableHeader = ['vrf', 'neighborIp', 'St/PfxRcd']
        self.contentDic = {}

    def getCommand(self):
        return 'show bgp vrf all ipv6 unicast summary'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStr = cmdStr.split('\nVRF: ')[1:]
            for vrf_output_str in cmdStr:
                vrf_output_str_lines = vrf_output_str.splitlines()
                vrf = vrf_output_str_lines[0].strip()
                '''
DSL_DATA
-------------
BGP VRF DSL_DATA, state: Active
BGP Route Distinguisher: 30722:1003201111
VRF ID: 0x60000002
BGP router identifier 10.176.1.13, local AS number 30722
Non-stop routing is enabled
BGP table state: Active
Table ID: 0xe0800001   RD version: 42927
BGP main routing table version 42927
BGP NSR Initial initsync version 486 (Reached)
BGP NSR/ISSU Sync-Group versions 42927/0

BGP is operating in STANDALONE mode.


Process       RcvTblVer   bRIB/RIB   LabelVer  ImportVer  SendTblVer  StandbyVer
Speaker           42927      42927      42927      42927       42927       42927

Neighbor        Spk    AS MsgRcvd MsgSent   TblVer  InQ OutQ  Up/Down  St/PfxRcd
2a01:8d0:e:4:1::
                  0 30722 2752831 1317015    42927    0    0    1d18h          1
2a01:8d0:f:0:1:7::
                  0 30722 1024285 1026313    42927    0    0    1d18h          0

                '''
                for line in re.findall('^[\d:a-f]+.*', vrf_output_str, re.M):
                    neighborIp = line.strip()
                    nextline = vrf_output_str_lines[vrf_output_str_lines.index(line)+1]
                    nextline = nextline.split()
                    st_pfxRcd = nextline[-1]
                    lineDic = {'vrf':vrf, 'neighborIp':neighborIp, 'St/PfxRcd':st_pfxRcd}
                    self.contentDic[(vrf, neighborIp)] = lineDic

