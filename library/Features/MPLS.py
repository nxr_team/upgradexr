import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class MPLS(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[MPLS]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getCommandClassList(self):
        return [ShoMplsTrafficEngTunnelsTabular]



######################################################################################################
######################################################################################################

class ShoMplsTrafficEngTunnelsTabular(MPLS, MotherCommand):
    def __init__(self):
        self.tableHeader = ['tunnelName', 'lspId', 'destinationAddress', 'sourceAddress', 'tunnelState', 'frrState', 'lspRole', 'pathProtection']
        self.contentDic = {}

    def getCommand(self):
        return 'show mpls traffic-eng tunnels tabular'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            '''
           Tunnel   LSP     Destination          Source    Tun    FRR  LSP  Path
             Name    ID         Address         Address  State  State Role  Prot
----------------- ----- --------------- --------------- ------ ------ ---- -----
    tunnel-te3110    82    10.176.1.159     10.176.0.75     up  Inact Head Ready
    tunnel-te3110    87    10.176.1.159     10.176.0.75     up  Inact Head Ready
            '''
            for line in re.findall('.*\s\d+\.\d+\.\d+\.\d+\s.*', cmdStr, re.M):
                lineArray = line.split()
                tunnelName = lineArray[0]
                lspId = 0
                if (tunnelName, lspId) in self.contentDic:
                    lspId = 1
                destinationAddress = lineArray[2]
                sourceAddress = lineArray[3]
                tunnelState = lineArray[4]
                frrState = lineArray[5]
                lspRole = lineArray[6]
                if len(lineArray) == 8:
                    pathProtection = lineArray[7]
                else:
                    pathProtection = 'None'
                lineDic = {'tunnelName':tunnelName, 'lspId':lspId,'destinationAddress':destinationAddress,
                           'sourceAddress':sourceAddress, 'tunnelState':tunnelState, 'frrState':frrState,
                           'lspRole':lspRole, 'pathProtection':pathProtection}
                self.contentDic[(tunnelName, lspId)] = lineDic

######################################################################################################
######################################################################################################
