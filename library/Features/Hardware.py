import re, os
import openpyxl
from .__Mother__ import MotherCheck, MotherCommand
from . import *
from .__Mother__ import GREEN, YELLOW, RED

class Hardware(MotherCheck):
    def __init__(self, showRunFormal, logDir):
        self.featureName = '[HW]'
        self.logDir = logDir
        #
        self.tableHeader = []
        self.contentDic = {}
        #
        self.commandCheckObjects = [CmdClass() for CmdClass in self.getCommandClassList()]

    def getCommandClassList(self):
        return [AdminShowPlatform, AdminShowRedundancy, AdminShowHWModuleFpdLocationAll, ShowMemorySummary, ShowInterfaceDescription]

######################################################################################################
######################################################################################################

class AdminShowPlatform(Hardware, MotherCommand):
    def __init__(self):
        self.tableHeader = ['node', 'state', 'configState']
        self.contentDic = {}

    def getCommand(self):
        return 'admin show platform'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            for line in re.findall('^0/.*', cmdStr, re.M):
                '''
0/RP0/CPU0      A99-RP2-SE(Active)        IOS XR RUN       PWR,NSHUT,MON
                '''
                line = [l for l in line.split('  ') if l]
                node = line[0]
                state = line[2]
                if len(line) == 4:
                    configState = line[3]
                else:
                    configState = ''
                lineDic = {'node':node, 'state':state, 'configState':configState}
                self.contentDic[node] = lineDic

######################################################################################################
######################################################################################################

class AdminShowRedundancy(Hardware, MotherCommand):
    def __init__(self):
        self.tableHeader = ['group', 'status']
        self.contentDic = {}

    def getCommand(self):
        return 'admin show redundancy'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStrLines = cmdStr.splitlines()
            row_Group = [line for line in cmdStrLines if re.search('^Group\s+', line, re.M)][0]
            row_Group_index = cmdStrLines.index(row_Group)
            row_Reload = [line for line in cmdStrLines if re.search('^Reload and\s+', line, re.M)][0]
            row_Reload_index = cmdStrLines.index(row_Reload)
            '''
Group            Primary         Backup          Status         
---------        ---------       ---------       ---------      
dsc              0/RP0/CPU0      0/RP1/CPU0      Ready          
dlrsc            0/RP0/CPU0      0/RP1/CPU0      Ready          
central-services 0/RP0/CPU0      0/RP1/CPU0      Ready          
v4-routing       0/RP0/CPU0      0/RP1/CPU0      Ready          
netmgmt          0/RP0/CPU0      0/RP1/CPU0      Ready          
mcast-routing    0/RP0/CPU0      0/RP1/CPU0      Ready          
v6-routing       0/RP0/CPU0      0/RP1/CPU0      Ready          

Reload and boot info
            '''
            for line in cmdStrLines[row_Group_index+2:row_Reload_index-1]:
                line = line.split()
                group = line[0]
                status = line[-1]
                lineDic = {'group': group, 'status': status}
                self.contentDic[group] = lineDic

######################################################################################################
######################################################################################################

class AdminShowHWModuleFpdLocationAll(Hardware, MotherCommand):
    def __init__(self):
        self.tableHeader = ['location', 'type', 'subtype', 'upd/dng']
        self.contentDic = {}

    def getCommand(self):
        return 'admin show hw-module fpd location all'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStr = cmdStr.split(' ====\n')[1]
            cmdStr = [s.strip() for s in cmdStr.split('--')[:-1] if s]
            for cardStr in cmdStr:
                location = ''
                '''
0/RP0/CPU0   A99-RP2-SE                 1.0   lc   cbc     0      35.14     No 
                                              lc   rommon  0      14.35     No 
                                              lc   fpga2   0       0.66     No 
                                              lc   fsbl    0       1.109    No 
                                              lc   lnxfw   0       1.109    No 
                                              lc   fpga3   0       0.16     No 
                                              lc   fpga4   0       0.16     No 
                                              lc   fpga5   0       0.12     No 
                                              lc   fpga6   0       0.08     No 
                '''
                for line in cardStr.splitlines():
                    line = line.split()
                    type = None
                    subtype = None
                    upd_dns = None
                    if len(line) == 8:
                        location = line[0]
                        type = line[3]
                        subtype = line[4]
                        upd_dns = line[7]
                    else:
                        type = line[0]
                        subtype = line[1]
                        upd_dns = line[4]
                    lineDic = {'location': location, 'type': type, 'subtype': subtype, 'upd/dng': upd_dns}
                    self.contentDic[(location, type, subtype)] = lineDic


######################################################################################################
######################################################################################################

class ShowMemorySummary(Hardware, MotherCommand):
    '''
    solo log
    '''
    def __init__(self):
        self.tableHeader = []
        self.contentDic = {}

    def getCommand(self):
        return 'show memory summary'

######################################################################################################
######################################################################################################

class ShowInterfaceDescription(Hardware, MotherCommand):
    def __init__(self):
        self.tableHeader = ['interfaceName', 'status', 'protocol']
        self.contentDic = {}

    def getCommand(self):
        return 'show interface description'

    def parseCommand(self, logDir):
        cmd = self.getCommand()
        with open(os.path.join(logDir, MotherCheck.getCmdName(cmd))) as f:
            cmdStr = f.read()
            cmdStrLines = cmdStr.splitlines()
            specialLine = [line for line in cmdStrLines if re.search('^-', line, re.M)][0]
            specialLineIndex = cmdStrLines.index(specialLine)
            '''
--------------------------------------------------------------------------------
BE6                up          up          Bundle 400Gb to MIVPE111
BE7                up          up          Bundle 300Gb to RMVPE211
            '''
            for line in cmdStrLines[specialLineIndex+1:-2]:
                line = line.split()
                interfaceName = line[0]
                status = line[1]
                protocol = line[2]
                lineDic = {'interfaceName': interfaceName, 'status': status, 'protocol': protocol}
                self.contentDic[interfaceName] = lineDic

    def compare(self, other):
        keys = sorted(self.contentDic.keys())
        resultsCompareList = []
        myList = []
        otherList = []
        for k in keys:
            if k in other.contentDic:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append(self.__dic2list__(other.contentDic[k], self.tableHeader))
                if self.contentDic[k] == other.contentDic[k]:
                    resultsCompareList.append(GREEN)
                elif (self.contentDic[k]['status']+self.contentDic[k]['protocol']).replace('admin-', '') == \
                        (other.contentDic[k]['status']+other.contentDic[k]['protocol']).replace('admin-', ''):
                    resultsCompareList.append(YELLOW)
                else:
                    resultsCompareList.append(RED)
            else:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append([])
                resultsCompareList.append(RED)
        return resultsCompareList, myList, otherList