import copy
from . import *
GREEN = 'green'
YELLOW = 'yellow'
RED = 'red'

class MotherCheck:
    def __init__(self):
        self.featureName = '###'
        self.logDir = ''
        self.commandCheckObjects = []

    def getFeatureName(self):
        return self.featureName

    def getCmdToDownload(self):
        return [cmdObj.getCommand() for cmdObj in self.commandCheckObjects]

    def getCmdToDownloadTupleList(self):
        return [(self.featureName, x) for x in self.getCmdToDownload()]

    @staticmethod
    def getCmdName(cmd):
        cmd = cmd.replace(' ', '_')
        cmd = cmd.replace('/', '_')
        cmd = cmd.replace(':', '_')
        cmd = cmd.replace('|', '_')
        return cmd + '.txt'

    def getParse(self):
        print('@' * 50)
        print('parsing {}'.format(self.featureName))
        #
        for cmdObj in self.commandCheckObjects:
            cmdObj.parseCommand(self.logDir)
        #
        print('end parsing')
        self.printParseCommands()
        print('@'*50)

    def getSheet(self, workbook, worksheetName=''):
        if not worksheetName:
            worksheetName = re.search('[A-Z]+', self.featureName).group(0)
            print(worksheetName)
        mysheet = workbook.create_sheet(worksheetName)
        for cmdObj in self.commandCheckObjects:
            mysheet.append([cmdObj.getCommand()])
            for lineVector in cmdObj.reprHeaderAndContent():
                mysheet.append(lineVector)
            ###
            mysheet.append([])
            mysheet.append([])
            mysheet.append([])
        return mysheet

    def compare__getSheet(self, other, workbook, vectorMeaning, worksheetName=''):
        if not worksheetName:
            worksheetName = re.search('[A-Z]+', self.featureName).group(0)
            print(worksheetName)
        mysheet = workbook.create_sheet(worksheetName)
        tmpList = []
        for myObj, otherObj in zip(self.commandCheckObjects, other.commandCheckObjects):
            mysheet.append([myObj.getCommand()])
            mysheet.append(['','']+myObj.tableHeader)
            #
            color, myLine, otherLine = myObj.compare(otherObj)
            for color_i, myLine_i, otherLine_i in zip(color, myLine, otherLine):
                mysheet.append([vectorMeaning[0], color_i] + myLine_i)
                mysheet.append([vectorMeaning[1], color_i] + otherLine_i)
            #
            if RED in color:
                tmpList.append((self.getFeatureName(), myObj.getCommand(), RED))
            elif YELLOW in color:
                tmpList.append((self.getFeatureName(), myObj.getCommand(), YELLOW))
            else:
                tmpList.append((self.getFeatureName(), myObj.getCommand(), GREEN))
            ###
            mysheet.append([])
            mysheet.append([])
            mysheet.append([])
        return {'sheet':mysheet, 'colorSetList':tmpList}

    def printParseCommands(self):
        for cmdObj in self.commandCheckObjects:
            print('-'*100)
            for lineVector in cmdObj.reprHeaderAndContent():
                print('\t'.join([str(x) for x in lineVector]))
        print('-' * 100)

class MotherCommand():
    def __init__(self):
        self.tableHeader = []
        self.contentDic = {}

    def getCommand(self):
        pass

    def parseCommand(self, logDir):
        pass

    @staticmethod
    def __dic2list__(dic, keys, default=None):
        r = []
        for k in keys:
            r.append(dic.get(k, default))
        return r

    def reprContent(self):
        returnList = []
        keys = sorted(list(self.contentDic.keys()))
        for k in keys:
            returnList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
        return returnList

    def reprHeaderAndContent(self):
        r = copy.deepcopy(self.reprContent())
        r.insert(0, self.tableHeader)
        #return self.tableHeader + self.reprContent()
        return r

    #def __eq__(self, other):
    #    if len(self.contentDic.keys()) != len(other.contentDic.keys()):
    #        return False
    #    for k in self.contentDic.keys():
    #        if k in other and self.contentDic(k) == other.contentDic(k):
    #            continue
    #        else:
    #            return False
    #    return True

    def compare(self, other):
        keys = sorted(self.contentDic.keys())
        resultsCompareList = []
        myList = []
        otherList = []
        for k in keys:
            if k in other.contentDic:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append(self.__dic2list__(other.contentDic[k], self.tableHeader))
                if self.contentDic[k] == other.contentDic[k]:
                    resultsCompareList.append(GREEN)
                else:
                    resultsCompareList.append(RED)
            else:
                myList.append(self.__dic2list__(self.contentDic[k], self.tableHeader))
                otherList.append([])
                resultsCompareList.append(RED)
        return resultsCompareList, myList, otherList
