# upgradeXR

Generazione dei comandi necessari all'isolamento
e alla successiva rimessa a traffico:

- shutdown accesso
- overload bit (ISIS isolation)
- shutdown core
- shut delle interfacce

Verifica dello stato pre/post upgrade:

- Hardware checks
- Adiacenze ISIS 
- Stato dei neighbor BGP
- Rotte per VRF

