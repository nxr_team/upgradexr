from library.Device import Device
from library.util import *
import os
from openpyxl import Workbook
'''
Menu di scelta per attiviita
'''

if __name__ == '__main__':
    routerName=input("Quale apparato devi aggiornare: ").upper()
    choice = ''
    while choice != 'exit':
        print('')
        print('"0" - Generate scripts')
        print('"1" - Data Collection pre-upgrade')
        print('"2" - Data Collection post-upgrade')
        print('"3" - Health check')
        print('"exit" - to quit')
        choice = input('Choice one: ')
        if choice == '0':
            device = Device(routerName, OLD)
            ## generate script module
            print(" Generazione degli script di isolamento per : ", routerName)
            shutCmdsList = []
            shutCmdsList = shutCmdsList + ['','','#---- ISOLAMENTO BGP ACCESSO ----#']
            shutCmdsList = shutCmdsList + device.edge_bgp_neighbor_obj.getShutCmds()
            shutCmdsList = shutCmdsList + ['', '', '#---- ISOLAMENTO BGP CORE ----#']
            shutCmdsList = shutCmdsList + device.core_bgp_neighbor_obj.getShutCmds()
            shutCmdsList = shutCmdsList + ['', '', '#---- SET OVERLOAD BIT ISIS ----#']
            shutCmdsList = shutCmdsList + device.isis_obj.getShutCmds()
            shutCmdsList = shutCmdsList + ['', '', '#---- ISOLAMENTO INTERFACCE ----#']
            shutCmdsList = shutCmdsList + device.interface_obj.getShutCmds()
            with open(os.path.join(device.deviceDirPath, 'shutCmdsList.txt'), 'w') as f:
                f.write('\n'.join(shutCmdsList))
            print(" Generazione degli script per rimessa in servizio : ", routerName)
            noShutCmdsList = []
            noShutCmdsList = noShutCmdsList + ['', '', '---- RIPRISTINO INTERFACCE ----']
            noShutCmdsList = noShutCmdsList + device.interface_obj.getNoShutCmds()
            noShutCmdsList = noShutCmdsList + ['', '', '---- UNSET OVERLOAD BIT ISIS ----']
            noShutCmdsList = noShutCmdsList + device.isis_obj.getNoShutCmds()
            noShutCmdsList = noShutCmdsList + ['', '', '---- RIPRISTINO BGP CORE ----']
            noShutCmdsList = noShutCmdsList + device.core_bgp_neighbor_obj.getNoShutCmds()
            noShutCmdsList = noShutCmdsList + ['', '', '---- RIPRISTINO BGP ACCESSO ----']
            noShutCmdsList = noShutCmdsList + device.edge_bgp_neighbor_obj.getNoShutCmds()
            with open(os.path.join(device.deviceDirPath, 'noShutCmdsList.txt'), 'w') as f:
                f.write('\n'.join(noShutCmdsList))
        elif choice == '1':
            ## collect module
            ## si puo' usare lo stesso modulo utilizzando suffisso _pre_
            print(" Collect data on "+routerName+" before upgrade ...")
            device = Device(routerName, OLD)
            device.downloadShowCommands()
            workbook = Workbook()
            device.parseCommands()
            device.parsedCommandsToSheets(workbook)
            workbook.save(os.path.join(device.deviceDirPath, 'PRE_CHECKs.xlsx'))
        elif choice == '2' :
            ## collect module
            ## si puo' usare lo stesso modulo utilizzando suffisso _post_
            print(" Collect data on "+routerName+" post upgrade ...")
            device = Device(routerName, NEW)
            device.downloadShowCommands()
            workbook = Workbook()
            device.parseCommands()
            device.parsedCommandsToSheets(workbook)
            workbook.save(os.path.join(device.deviceDirPath, 'POST_CHECKs.xlsx'))
        elif choice == '3':
            ## compare data to check successfull upgrade
            print(" Check health after upgrade")
            deviceOld = Device(routerName, OLD)
            deviceOld.parseCommands()
            deviceNew = Device(routerName, NEW)
            deviceNew.parseCommands()
            deviceOld.compare(deviceNew)


